from elevator.core import *


def print_status(controller):
    elevator = controller.elevators[0]
    print('--------Status of elevator 0', elevator.status)
    print('[Current floor]', elevator.current_floor, ' of (0 -', elevator.number_of_floors-1, ')')
    print('[Floor selections]')
    print(elevator.select)
    print('[Floor calls]')
    for f in elevator.floors[::-1]:
        print('\t', f.call)


def input_floor_selections(number_of_floors):
    selection = []
    while len(selection) == 0:
        str_selection = input('\tplease type in comma-delimited string of integers as the floor selection\t')
        try:
            selection = [i for i in [int(i) for i in str_selection.split(',')] if 0 <= i < number_of_floors]
        except Exception as e:
            print('got illegal input ', str_selection)
    print(selection)
    return selection


def direction_translate(direction_str):
    if direction_str == 'up':
        return Direction.UP
    elif direction_str == 'down':
        return Direction.DOWN


def input_calls(number_of_floors):
    calls = {}
    while True:
        str_selection = input(
            '\tplease type in calls in the format of [floor_number]|<up>,<down>, or type in \'f\' to finish\t')
        if str_selection == 'f':
            return calls
        try:
            floor_str, call_string = str_selection.split('|')
            floor_no = int(floor_str)
            assert 0 <= floor_no < number_of_floors
            if floor_no == 0:
                assert 'down' not in call_string
            if floor_no == number_of_floors -1:
                assert 'up' not in call_string

            calls_list = [d for d in call_string.split(',') if d in ['up', 'down']]
            calls_list = list(map(direction_translate, calls_list))
            calls[floor_no] = calls_list
        except Exception as e:
            print('got illegal input ', str_selection)


def main():
    controller = Controller(1, 5)

    usage = \
        """
        This is the simple elevator simulator. It will show you the inside-car-customer floor selections, external calls, 
        planned stops, elevator status, and also it's actions. 
        It assumes the elevator update status at each time tick, the elevator goes up or down one floor at each time tick. 
        User can interact with the simulator by type in a list of floor selections and external calls at each time tick, 
        or just press entry key to let the simulator continue.
        """
    print(usage)
    print()

    while True:
        print()
        print_status(controller)
        ipt = input('want to update[u], continue[c] or quit[q]?\t')
        if ipt not in ['u', 'c', 'q']:
            print('please type in u/c/q')
        elif ipt == 'q':
            print('Bye')
            exit(0)
        elif ipt == 'u':
            print('sure, you want to update data.')
            selection = input_floor_selections(controller.elevators[0].number_of_floors)
            calls = input_calls(controller.elevators[0].number_of_floors)
            controller.elevators[0].on_select(list(set(selection).union(set(controller.elevators[0].select))))
            for k in calls:
                controller.on_call(k, list(set(calls[k]).union(controller.elevators[0].floors[k].call)))
        direction, journey = controller.plan_journey()

        print('--------Here is the planned journey')
        print('Direction:', direction, '\tstops', journey)

        action = controller.move(direction, journey)
        print('--------Here\'s the action')
        print('\t', action)


if __name__ == "__main__":
    main()
