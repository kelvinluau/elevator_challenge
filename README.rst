Elevator Simulator Project
========================

Business Rules
---------------

This code assumes the following rules:
    1. when the elevator is still, it choose the direction with the most requests.
    2. valid requests include all the selected floors in the same direction from the current floor plus all the calls goes to the same direction, plus the call goes to the opposite direction from the far end of the direction.
    3. when the elevator stops at a floor, it will clean up selection of the current floor and call made from the same floor asks for the same direction.
    4. when the elevator has no further stop to go in certain direction, it will clean up the call of both directions.
    5. when there's no selection and call, the elevator will stay at where it stops.
    6. the elevator moves up or down by one level at a time tick; it also check user input at the period of one time tick.
    7. the elevator can re-schedule its journey plan at any time tick without considering more complicated scenarios like elevator accelerating and slowing down, near floor input freezing, etc.

Code Structure
---------------

Folder /elevator contains the module of elevator;
Folder /tests contains the unit tests
simple_elevator_simulator.py is a command line interactive simulator.

There're three classes in the code:
The Floor class hold the building floor information and the calls made on that floor.
The Elevator class consists of a list of Floors, and also floor selections made by in-car passengers.
The Controller class is responsible for controlling the Elevator behavior. The controller hold a list of elevators so
it potentially can deal with multiple-elevator scenario. But because of time limit and the complexity of business rules,
multiple-elevator version is not implemented. The controller can only work with a single elevator.

Packaging and installation
---------------

The module was developed using Anaconda latest version without any additional dependency. It can be distributed and installed as wheel package.

How to test using interactive simulator
---------------

Please run the simulator by:

    python simple_elevator_simulator.py


The code run in a dead loop. It prints out the current status, planned journey, and also the movement. It accept 3 inputs:
  * 'u': feed in floor selection and calls;
  * 'c': continue -- let the elevator to step over;
  * 'q': quit from code execution.

When type 'u' to feed in passenger floor selection and calls, there're three further steps:
  * type in a comma-separated list of integers as the floor selection. In the case the input is invalid, the script will ask user to retry;
  * type in a call with the format: [floor_number]|<up>, <down>.
  * at step 2, user can opt to type in 'f' to finish the data entry and return to the main process.