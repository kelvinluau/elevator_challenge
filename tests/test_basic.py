# -*- coding: utf-8 -*-
from .context import elevator
from elevator.core import *

import unittest


class BasicFloorTestSuite(unittest.TestCase):
    """Basic test cases."""

    def setUp(self) -> None:
        self.stub = Floor(12, True)

    def test_floor_clean_up_UP(self):
        self.stub.on_call([Direction.UP, Direction.DOWN])

        assert self.stub.floor_number == 12
        assert self.stub.enabled
        assert self.stub.call == [Direction.UP, Direction.DOWN]

        self.stub.on_clean_up(Direction.UP)
        assert self.stub.call == [Direction.DOWN]

    def test_floor_clean_up_DOWN(self):
        self.stub.on_call([Direction.UP, Direction.DOWN])

        assert self.stub.floor_number == 12
        assert self.stub.enabled
        assert self.stub.call == [Direction.UP, Direction.DOWN]

        self.stub.on_clean_up(Direction.DOWN)
        assert self.stub.call == [Direction.UP]

    def test_floor_clean_up_EMPTY(self):
        assert self.stub.floor_number == 12
        assert self.stub.enabled
        assert self.stub.call == []

        self.stub.on_clean_up(Direction.DOWN)
        assert self.stub.call == []


class BasicElevatorTestSuite(unittest.TestCase):
    """Basic test cases."""

    def setUp(self):
        self.stub = Elevator(1, 5)

    def test_elevator_creation(self):
        assert len(self.stub.floors) == 5
        assert self.stub.current_floor == 0
        assert self.stub.number_of_floors == 5
        assert self.stub.status == Direction.STILL
        assert self.stub.select == []

    def test_on_select_wrong_number(self):
        with self.assertRaises(AssertionError):
            self.stub.on_select([2, 3, 10])

    def test_on_select_negative_number(self):
        with self.assertRaises(AssertionError):
            self.stub.on_select([-1, 2, 3])

    def test_on_select_floors(self):
        self.stub.on_select([1, 2, 3, 4])
        assert self.stub.select == [1, 2, 3, 4]

    def test_on_call_too_large(self):
        with self.assertRaises(AssertionError):
            self.stub.on_call(5, [Direction.UP])

    def test_on_call_go_up_from_top_floor(self):
        with self.assertRaises(AssertionError):
            self.stub.on_call(4, [Direction.UP])

    def test_on_call_go_down_from_ground_floor(self):
        with self.assertRaises(AssertionError):
            self.stub.on_call(0, [Direction.DOWN])

    def test_on_call_too_small(self):
        with self.assertRaises(AssertionError):
            self.stub.on_call(-1, [Direction.UP])

    def test_on_call_wrong_selection(self):
        with self.assertRaises(AssertionError):
            self.stub.on_call(3, ['random'])

    def test_on_call_go_up(self):
        self.stub.on_call(3, [])
        assert self.stub.floors[3].call == []

    def test_on_clean_up_UP(self):
        self.stub.on_select([1, 3])
        self.stub.on_call(2, [Direction.UP, Direction.DOWN])
        self.stub.on_call(3, [Direction.UP, Direction.DOWN])

        assert self.stub.select == [1, 3]
        assert self.stub.floors[2].call == [Direction.UP, Direction.DOWN]
        assert self.stub.floors[3].call == [Direction.UP, Direction.DOWN]

        self.stub.current_floor = 2
        self.stub.on_clean_up(Direction.UP)
        assert self.stub.select == [1]
        assert self.stub.floors[2].call == [Direction.DOWN]
        assert self.stub.floors[3].call == [Direction.DOWN]

    def test_on_clean_up_DOWN(self):
        self.stub.on_select([1, 3])
        self.stub.on_call(2, [Direction.UP, Direction.DOWN])
        self.stub.on_call(3, [Direction.UP, Direction.DOWN])

        assert self.stub.select == [1, 3]
        assert self.stub.floors[2].call == [Direction.UP, Direction.DOWN]
        assert self.stub.floors[3].call == [Direction.UP, Direction.DOWN]

        self.stub.current_floor = 2
        self.stub.on_clean_up(Direction.DOWN)
        assert self.stub.select == [3]
        assert self.stub.floors[2].call == [Direction.UP]
        assert self.stub.floors[3].call == [Direction.UP]


class BasicControllerTestSuite(unittest.TestCase):
    """Basic test cases."""

    def setUp(self):
        self.stub = Controller(1, 5)

    def test_controller(self):
        assert len(self.stub.elevators) == 1
        assert len(self.stub.elevators[0].floors) == 5
        assert self.stub.elevators[0].status == Direction.STILL

    def test_decide_on_direction_up(self):
        self.stub.on_call(3, [Direction.UP])

        assert self.stub.elevators[0].floors[3].call == [Direction.UP]
        assert self.stub.elevators[0].status == Direction.STILL
        assert self.stub.decide_on_direction() == Direction.UP

    def test_decide_on_direction_down(self):
        self.stub.on_call(3, [Direction.UP])
        self.stub.elevators[0].current_floor = 4

        assert self.stub.elevators[0].floors[3].call == [Direction.UP]
        assert self.stub.elevators[0].status == Direction.STILL
        assert self.stub.decide_on_direction() == Direction.DOWN

    def test_decide_on_direction_turn_up(self):
        self.stub.on_call(3, [Direction.UP])
        self.stub.elevators[0].current_floor = 2
        self.stub.elevators[0].status = Direction.DOWN

        assert self.stub.elevators[0].floors[3].call == [Direction.UP]
        assert self.stub.elevators[0].status == Direction.DOWN
        assert self.stub.decide_on_direction() == Direction.UP

    def test_decide_on_direction_turn_still(self):
        # self.stub.on_call(3, [Direction.UP])
        self.stub.elevators[0].current_floor = 4
        self.stub.elevators[0].status = Direction.DOWN

        # assert self.stub.elevators[0].floors[3].call == [Direction.UP]
        assert self.stub.decide_on_direction() == Direction.STILL

    def test_stop(self):
        self.stub.elevators[0].current_floor = 3
        self.stub.elevators[0].on_call(3, [Direction.UP, Direction.DOWN])
        self.stub.elevators[0].on_select([1, 3, 4])

        self.stub.stop(Direction.UP, [1,2,3])

        assert self.stub.elevators[0].select == [1, 4]
        assert self.stub.elevators[0].floors[3].call == [Direction.DOWN]

    def test_plan_journey(self):
        self.stub.elevators[0].on_call(3, [Direction.DOWN])
        self.stub.elevators[0].on_select([1, 4])
        self.stub.elevators[0].current_floor = 2
        direction, journey = self.stub.plan_journey()
        assert direction == Direction.UP
        assert journey == [4]


if __name__ == '__main__':
    unittest.main()
