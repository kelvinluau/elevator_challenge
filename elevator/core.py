# -*- coding: utf-8 -*-
from enum import Enum

Direction = Enum('Direction', 'UP DOWN STILL')


class Floor(object):
    def __init__(self, floor_number: int, enabled: bool):
        self.floor_number = floor_number
        self.enabled = enabled
        self.call = []

    # for on-floor passenger to make calls
    def on_call(self, list_of_call: list):
        self.call = list_of_call

    # reset call status
    def on_clean_up(self, direction: Direction):
        if direction == Direction.UP:
            self.call = [c for c in self.call if c != Direction.UP]
        else:
            self.call = [c for c in self.call if c != Direction.DOWN]


class Elevator(object):

    def __init__(self, elevator_number, number_of_floors: int):
        self.elevator_number = elevator_number
        self.number_of_floors = number_of_floors
        self.floors = [Floor(k, True) for k in range(number_of_floors)]
        self.current_floor = 0
        self.select = []
        self.status = Direction.STILL

    # for in-car passengers to make floor selection
    def on_select(self, selections):
        assert set(selections) == set([f for f in selections if f in range(self.number_of_floors)])
        self.select = selections

    # keeping record of calls made by on-floor passengers
    def on_call(self, floor, calls):
        assert floor < self.number_of_floors
        assert floor >= 0
        assert set(calls).union({Direction.UP, Direction.DOWN}) == {Direction.UP, Direction.DOWN}
        if floor == self.number_of_floors - 1:
            assert Direction.UP not in calls
        if floor == 0:
            assert Direction.DOWN not in calls
        self.floors[floor].on_call(calls)

    # reset selection and call records
    def on_clean_up(self, direction: Direction):
        for f in self.floors:
            f.on_clean_up(direction)
        if direction == Direction.UP:
            self.select = [s for s in self.select if s < self.current_floor]
        elif direction == Direction.DOWN:
            self.select = [s for s in self.select if s > self.current_floor]


class Controller(object):

    def __init__(self, num_elevator, num_floors):
        self.elevators = [Elevator(e, num_floors) for e in range(num_elevator)]

    def on_call(self, floor, calls):
        for e in self.elevators:
            e.on_call(floor, calls)

    # produce stops if elevator goes up
    def stops_up(self):
        result = [f for f in range(self.elevators[0].current_floor + 1, self.elevators[0].number_of_floors)
                  if Direction.UP in self.elevators[0].floors[f].call or f in self.elevators[0].select]

        if len(result) == 0:
            high_mark = self.elevators[0].current_floor + 1
        else:
            high_mark = max(result)
        calls = [f for f in range(high_mark, self.elevators[0].number_of_floors)
                       if Direction.DOWN in self.elevators[0].floors[f].call]

        if len(calls) >0:
            result.append(max(calls))
        return list(set(result))

    # produce stops if elevator goes down
    def stops_down(self):
        result = [f for f in range(self.elevators[0].current_floor)
                  if Direction.DOWN in self.elevators[0].floors[f].call or f in self.elevators[0].select]
        if len(result) ==0:
            low_mark = self.elevators[0].current_floor
        else:
            low_mark = min(result)

        calls = [f for f in range(low_mark) if Direction.UP in self.elevators[0].floors[f].call]


        if len(calls) >0:
            result.append(max(calls))
        return list(set(result))

    # according to the elevator running status and selection and call records, decide on the moving direction
    def decide_on_direction(self):
        num_requests_up = len(self.stops_up())
        num_requests_down = len(self.stops_down())

        if num_requests_up == num_requests_down == 0:
            return Direction.STILL
        elif self.elevators[0].status == Direction.STILL:
            if num_requests_down > num_requests_up:
                return Direction.DOWN
            elif num_requests_down <= num_requests_up:
                return Direction.UP
            else:
                return Direction.STILL
        elif self.elevators[0].status == Direction.UP:
            if num_requests_up == 0:
                if num_requests_down > 0:
                    return Direction.DOWN
                else:
                    return Direction.STILL
            else:
                return Direction.UP

        elif self.elevators[0].status == Direction.DOWN:
            if num_requests_down == 0:
                if num_requests_up > 0:
                    return Direction.UP
                else:
                    return Direction.STILL
            else:
                return Direction.DOWN

    # execution of stopping at one floor.
    def stop(self, direction, journey):
        if self.elevators[0].current_floor in self.elevators[0].select:
            self.elevators[0].select.remove(self.elevators[0].current_floor)
        if direction in self.elevators[0].floors[self.elevators[0].current_floor].call:
            self.elevators[0].floors[self.elevators[0].current_floor].call.remove(direction)
        if len(journey) == 1:
            self.elevators[0].floors[self.elevators[0].current_floor].call = []
        return 'stop at level %d ... open_door... wait... close_door' % self.elevators[0].current_floor

    # drive the elevator according to the planned journey and direction
    def move(self, direction, journey):
        self.elevators[0].status = direction

        if direction == Direction.UP:
            self.elevators[0].current_floor = min(self.elevators[0].current_floor + 1,
                                                  self.elevators[0].number_of_floors - 1)
            action = 'Up_1'
        elif direction == Direction.DOWN:
            self.elevators[0].current_floor = max(self.elevators[0].current_floor - 1, 0)
            action = 'Down_1'
        else:
            action = 'Stand by'
        self.elevators[0].select = [s for s in self.elevators[0].select if s != self.elevators[0].current_floor]

        if self.elevators[0].current_floor in journey:
            action = self.stop(self.elevators[0].status, journey)
        return action

    # make plan of elevator run journey in one direction
    def plan_journey(self):
        direction = self.decide_on_direction()
        journey = None
        if direction == Direction.UP:
            journey = self.stops_up()
        elif direction == Direction.DOWN:
            journey = self.stops_down()
        else:
            journey = []
        return direction, journey
